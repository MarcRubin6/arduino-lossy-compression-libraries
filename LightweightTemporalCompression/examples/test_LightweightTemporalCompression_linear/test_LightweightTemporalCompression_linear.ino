#include <LightweightTemporalCompression.h>

#define SIZE 4
#define K 75

int array[SIZE];

void setup() {
  Serial.begin(57600); 

  // fill the array with a sine wave
  set_input(); 

  //compress using KRLE (this prints);
  compress(array, K, SIZE);

}

void loop() {

}

void set_input(){
  array[0] = 100;
  array[1] = 200;
  array[2] = 150;
  array[3] = 500;
}

