x.txt: 
- original signal (in ASCII)

y.txt: 
- LTC encoding (ASCII) from Arduino  
- format: "number:count" --> e.g., 64:4 

decode.py: 
- takes y.txt on standard input (ASCII)
- decoding written to standard output (ASCII)
- usage: UNIX> cat y.txt | ./decode.py > xhat.txt

xhat.txt:
- decoded estimate of signal x

decode_plot.m: 
- Matlab script: runs decoder, plots x, xhat.
- expects the above files
