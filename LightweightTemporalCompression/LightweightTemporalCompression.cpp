#include "LightweightTemporalCompression.h"

//specify the allowable e value for LTC;
//print encoding
void compress(int *input, int e, int SIZE){
    
    int count = 2;
    
    // 1. Initialization: Get the first data point, store into z.
    double tz = 0;
    int z = input[int(tz)];
    
    //Get next data point (t2,v2),
    double t = 1;
    int v2 = input[int(t)];
    
    //use it to initialize limits UL (UL is set to (t2,v2 +e)) and LL (LL is set to (t2,v2 −e).
    int UL = v2 + e;
    int LL = v2 - e;
    
    // 2. Calculate the highLine to be the line connecting z and UL.
    // m = (y2 - y1) / (t2 - t1)
    double mUL = (UL - z) / (t - tz);
    double bUL = UL - mUL * t;
    
    // 3. Calculate the lowLine to be the line connecting z and LL.
    double mLL = (LL - z) / (t - tz);
    double bLL = LL - (mLL * t);
    
    // 4. Get next data point.
    for(int t = 2; t < SIZE; t++){
        
        int v = input[t];
        
        // Transform the point to a ver- tical segment using the margin e.
        // Let ul be the highest point of the segment.
        // Let ll be the lowest point of the segment.
        int ul = v + e;
        int ll = v - e;
        
        double yUL = mUL*t + bUL;
        double yLL = mLL*t + bLL;
        
        // 5. If highLine is below the ll or if the lowLine is above the ul then goto 9,
        if (yUL < ll | yLL > ul){
            
            // 9. Cap off: output z to the output data stream.
            Serial.print(z);
            Serial.print(':');
            Serial.println(count);
            
            count = 1;
            
            // 10. Set z to be the point midway between UL and LL.
            z = (UL + LL) / 2;
            tz = t - 1;
            
            // 11. Set UL to be ul.
            UL = ul;
            
            // 12. Set LL to be ll.
            LL = ll;
            
            // 13. Goto 2
            // 2. Calculate the highLine to be the line connecting z and UL.
            // m = (y2 - y1) / (t2 - t1)
            mUL = (UL - z) / (t - tz);
            bUL = UL - mUL * t;
            
            // 3. Calculate the lowLine to be the line connecting z and LL.
            mLL = (LL - z) / (t - tz);
            bLL = LL - (mLL * t);
            
        } else { //continue onto the next step
            count = count + 1;
            
            // 6. If highLine goes above ul then set UL to be ul.
            if(yUL > ul){
                UL = ul;
            }
            
            // 7. If lowLine goes below ll then set LL to be ll.
            if(yLL < ll){
                LL = ll;
            }
            
            // 8. Goto 2.
            // 2. Calculate the highLine to be the line connecting z and UL.
            // m = (y2 - y1) / (t2 - t1)
            mUL = (UL - z) / (t - tz);
            bUL = UL - mUL * t;
            
            // 3. Calculate the lowLine to be the line connecting z and LL.
            mLL = (LL - z) / (t - tz);
            bLL = LL - (mLL * t);
            
        }
    }
    Serial.print(z);
    Serial.print(':');
    Serial.println(count);
    
}
