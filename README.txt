Lossy compression algorithms implemented for Arduino.  
	See DCOSS paper: "" for more details about each
	algorithm as well as comparative analysis.

These libraries are implemented using ASCII (not binary) transmissions 
	and are intended for educational purposes only.  

- RandomizedTimingVector
- LightWeightTemporalCompression
- K_RunLengthEncoding
- WaveletQuantizeThreshold_RLE
- LowPassFilteredFastFourierTransform

Each library contains an example Arduino sketch and a 
	variety of software to decode the ASCII encodings.

Note that these libraries can easily be modified to use more 
	efficient binary transmissions. The results listed in 
	the DCOSS paper above used binary transmissions as well 
	as XBee radio sleep to minimize packet size and maximize 
	power savings.



