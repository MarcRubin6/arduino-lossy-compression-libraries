#include "K_RunLengthEncoding.h"

//compress using KRLE, print running encoding
void compress(int *input, int k, int SIZE)
{
    int previous = input[0];
    int count = 1;
    
    for(int i = 1; i < SIZE; i++){
        
        //check for redundancy
        if( (previous - k) <= input[i] && input[i] <= (previous + k)){
            count++;
            
        } else { //new byte in sequence
            
            Serial.print(previous);
            Serial.print(":");
            Serial.println(count);
            
            //reset counter and redundant byte
            previous = input[i];
            count = 1;
        }
    }
    
    //what's left over 
    Serial.print(previous);
    Serial.print(":");
    Serial.println(count);
    
}
