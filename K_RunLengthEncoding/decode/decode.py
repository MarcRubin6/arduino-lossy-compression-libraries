#!/usr/bin/env python

import sys

#read in every line for standard input
for line in sys.stdin:
    
    #strip the newline character 
    tokens = line.strip(',\n\r').split(':');
    
    num =  tokens[0];
    count = int(tokens[1]);
        
    #formulate the decoded string
    for i in range(count):
        print num;

