close all; clear all; clc;

%decode signal y into estimate of x (xhat)
system('cat y.txt | ./decode.py > xhat.txt');

%compare xhat to original signal x
x = dlmread('x.txt');
xhat = dlmread('xhat.txt');

figure;
plot(x);
hold on;
plot(xhat,'r--');
