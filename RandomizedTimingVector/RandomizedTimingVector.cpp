#include <RandomizedTimingVector.h>

RandomizedTimingVector::RandomizedTimingVector(int compression_rate, int size)
{
    double perc = (100.0 - compression_rate) / 100;
    
    N = size;
    M = floor(N * perc);
    
    Phi = (byte *) malloc(sizeof(byte)*N);
    if(Phi == NULL){
        Serial.println("ERROR: could not allocate Phi");
        while(1);
    }
    
    genPhi(0);
    
}

void RandomizedTimingVector::printPhi()
{
    //print Phi
    for(int n = 0; n < N; n++){
        Serial.println(Phi[n]);
    }
}

void RandomizedTimingVector::genPhi(int seed)
{    
    int n;
    
    //generate sensing matrix
    randomSeed(seed);
    for(n = 0; n < M; n++){
        Phi[n] = 1;
    }
    for(; n < N; n++){
        Phi[n] = 0;
    }
    
    //user Fisher-Yates to randomly shuffle the timing vector
    for(int n = N - 1; n >= 1; n--){
        int j = random(n);
        int tmp = Phi[n];
        Phi[n] = Phi[j];
        Phi[j] = tmp;
    }
}

void RandomizedTimingVector::compress(int *input)
{
    for(int i = 0; i < N; i++){
        if(Phi[i] > 0){
            Serial.println(input[i]);
        }
    }
}
