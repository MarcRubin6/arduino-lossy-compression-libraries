close all; clear all; clc;

%change this for different size CS windows
N = 256;
x = dlmread('x.txt');

%do compressive sensing
y = dlmread('y.txt');

timing_vector = dlmread('phi.txt');
M = length(find(timing_vector==1));

%create Phi based on timing vector
Phi = zeros(M,N);
m = 1;
for n=1:N
    if(timing_vector(n) == 1)
       Phi(m,n) = 1; 
       m = m + 1;
    end
end

%generate measurement matrix
Psi = inv(dftmtx(N));
A = Phi*Psi;

%Calculating Initial guess
%y = A*alpha => linear algebra...
alpha = A'*y;

%get recovered alpha coefficients
alphap = l1eq_pd(alpha,A,[],y, 1e-3);

%estimate entire signal
xhat = real(ifft(alphap));

figure; 
plot(x);
hold on;
plot(xhat,'r--');
