x.txt: 
- original signal (in ASCII)

y.txt: 
- RTV encoding (ASCII) from Arduino  

phi.txt
- timing vector (of length N) used to represent Phi in RTV algorithm

xhat.txt:
- decoded estimate of signal x

decode_RTV.m: 
- requires l1-magic compressive sampling library (installed for Matlab).
- Matlab script: runs decoder, plots x, xhat.
- expects the above files

CR_15.jpg:
- 15% compression rate 

CR_50.jpg
- 50% compression rate

CR_85.jpg
- 85% compression rate
