#include "Arduino.h"


class RandomizedTimingVector {
    public:
        RandomizedTimingVector(int compression_rate, int size);
    
        void genPhi(int seed);
        void printPhi();
    
        void compress(int *input);
    
    private:
        int M;
        int N;
    
        //timing vector 
        byte *Phi;
};