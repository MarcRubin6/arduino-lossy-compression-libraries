clear all; close all; clc;

%user specified compression rate!!!
CR = 50;

%sampling rate (in Hz)
Fs = 50;

%size of FFT
N = 256;

%how big is the low pass filter?
perc = (100-CR) / 100;
num = floor((N/2)*perc);

%get encoding
y = dlmread('y.txt');

%to simulate integer precision.
y = y * N;

%get the real components and mirror
R = y(1:num);
R2 = zeros(N,1);
R2(1:num) = R;
R2(N/2+2:end) = flipud(R2(2:N/2));

%get the imagiary components and mirror anti-symmetric
I = -1*y(num+1:end); %flip 
I2 = zeros(N,1);
I2(1:num) = I;
I2(N/2+2:end) = -1*flipud(I2(2:N/2));

%formulate the transform coefficients and compute inverse
C = complex(R2,I2);
xhat = real(ifft(C,N));
dlmwrite('xhat.txt', xhat);

%plot original and estimate
figure;
subplot(211);
x = dlmread('x.txt');
plot(x);
hold on;
plot(xhat,'r--');

subplot(212);
F = fft(x,N);
plot(sqrt(real(F).^2 + imag(F).^2));
hold on;
F = fft(xhat,N);
plot(sqrt(real(F).^2 + imag(F).^2),'r--');



