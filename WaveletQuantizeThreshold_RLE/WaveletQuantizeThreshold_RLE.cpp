#include <WaveletQuantizeThreshold_RLE.h>

//help functions
void rle(double *input, int SIZE);
void isort(double *a, int n);
void threshold(double *d, double *copy, byte M, int N);

//perc is the desired compression rate, from 0 (no compression) to 99 (aggresive)
void compress(int* x, double perc, int N)
{
    double a;
    double d[N/2];
    //double s[N/2];
    double *s;
    double copy[N/2];
    int tmp;
    
    byte M = floor(N/2.0 * (1.0-perc));
    
    int i;
    //predict 1
    
    // Predict 1
    //d vector gets -0.5
    //a = -0.5;
    for (i=1; i < N-2; i+=2)
    {
        //x[i] += a* (x[i-1]+x[i+1]);
        tmp = x[i-1]+x[i+1];
        tmp >>= 1;
        tmp *= -1;
        x[i] += tmp;
        
    }
    //x[N-1]+=2*a*x[N-2];
    tmp = x[N-2];
    tmp >>= 1;
    tmp *= 2;
    x[N-1] = tmp;
    
    // Update 1
    //s vector gets -0.25
    //a=-0.25;
    for (i=2;i<N;i+=2)
    {
        //x[i] += a*((-1*x[i-1])-x[i+1]);
        tmp = (-1*x[i-1]) - x[i+1];
        tmp >>= 2;
        tmp *= -1;
        x[i] += tmp;
    }
    //x[0] += 2*a*x[1];
    tmp = x[1];
    tmp >>= 2;
    tmp *= 2;
    x[0] += tmp;
    
    // Scale
    //a=1/1.149604398;
    a = 1.0/1.4142135623731;
    //for (i=0;i<N;i++)
    for(i = 1; i < N; i+=2)
    {
        d[i/2] = (double) x[i] * a;
    }
    
    //copy d to copy vector
    for(int i = 0; i < N/2; i++){
        copy[i] = abs(d[i]);
    }
    //sort the copy
    isort(copy, N/2);
    
    //find the top percent, zero out others
    threshold(d, copy, M, N/2);
    
    //perform run length encoding
    rle(d, N/2);
    
    s = d;
    for(i = 0; i < N; i+=2)
    {
        s[i/2] = (double) x[i] / a;
    }
    
    //copy s to copy vector
    for(i = 0; i < N/2; i++){
        copy[i] = abs(s[i]);
    }
    
    //sort the copy
    isort(copy, N/2);
    
    //find the top percent, zero out others
    threshold(s, copy, M, N/2);
    
    //perform run length encoding
    rle(s, N/2);
}

void threshold(double *d, double *copy, byte M, int N)
{
    for(int i = 0; i < N; i++){
        byte found = 0;
        for(int j = N-1; j >= N-M; j--){
            if(abs(d[i]) == copy[j]){
                found = 1;
            }
        }
        if(found == 0){
            d[i] = 0;
        }
    }
}

//run length encoding
void rle(double *input, int SIZE)
{
    double previous = input[0];
    byte count = 1;
    
    for(int i = 1; i < SIZE; i++){
        
        //check for redundancy
        if(input[i] == previous) {
            count++;
        } else { //new byte in sequence
            
            Serial.print(previous,3);
            Serial.print(':');
            Serial.println(count);
            
            previous = input[i];
            count = 1;
        }
    }
    
    Serial.print(previous,3);
    Serial.print(':');
    Serial.println(count);
}

//insertion sort
void isort(double *a, int n)
{
    for (int i = 1; i < n; ++i)
    {
        double j = a[i];
        int k;
        for (k = i - 1; (k >= 0) && (j < a[k]); k--)
        {
            a[k + 1] = a[k];
        }
        a[k + 1] = j;
    }
}