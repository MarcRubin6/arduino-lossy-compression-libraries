clear all; close all; clc;

%decode the run length encoded wavelet coefficients
system('cat y.txt | ./decode.py > decode.txt');
Y = dlmread('decode.txt');
N = length(Y);

%get the 'd' vector (1st half of wavelet coefficients)
d = Y(1:N/2);
dlmwrite('d.txt',d);

%get the 's' vector (2nd half of wavelet coefficients)
s = Y(N/2+1:end);
dlmwrite('s.txt',s);

%perform inverse wavelet transform
system(sprintf('./icdf22 s.txt d.txt %d > xhat.txt',N));

x = dlmread('x.txt');
xhat = dlmread('xhat.txt');

figure;
plot(x);
hold on;
plot(xhat,'r--');

system('rm d.txt s.txt decode.txt');