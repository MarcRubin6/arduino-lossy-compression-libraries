#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <fstream>
using namespace std;

int SIZE = 128;
#define HUGE 1024

double d[HUGE];
double s[HUGE];
double finalX[HUGE];

double *tempbank;

void iwt97(int n)
{
		double a;
		double x[SIZE]; 

		int i;
		// Unpack
		if (tempbank==0) tempbank=(double *)malloc(n*sizeof(double));
		for (i=0;i<n/2;i++)
		{
				tempbank[i*2]=s[i];
				tempbank[i*2+1]=d[i];
		}
		for (i=0;i<n;i++) x[i]=tempbank[i];
		// Undo scale
		//a=1.149604398;
		a = 1.4142135623731;
		for (i=0;i<n;i++)
		{
				if (i%2) x[i]*=a;
				else x[i]/=a;
		}

		// Undo update 1
		//a=0.05298011854;
		a = 0.25;
		for (i=2;i<n;i+=2)
		{
				x[i]+=a*(x[i-1]+x[i+1]);
		}
		x[0]+=2*a*x[1];

		// Undo predict 1
		//a=1.586134342;
		a = 0.5;
		for (i=1;i<n-2;i+=2)
		{
				x[i]+=a*(x[i-1]+x[i+1]);
		} 
		x[n-1]+=2*a*x[n-2];

		for(i=0; i < n; i++){
				finalX[i] = x[i];
		}
}

int main(int argc, char **argv)
{
		if(argc != 4){
				cerr << "USAGE: ./icdf22 s.txt d.txt N\n";
				exit(1);
		}

		SIZE = atoi(argv[3]);

		int i;
		
		//read in s
		ifstream inputFile;
		inputFile.open(argv[1]);
		for(i = 0; i < SIZE/2; i++){
				inputFile >> s[i];
		}
		inputFile.close();

		//read in d
		inputFile.open(argv[2]);
		for(i = 0; i < SIZE/2; i++){
				inputFile >> d[i];
		}
		inputFile.close();

		//compute inverse
		iwt97(SIZE);

		for (i=0;i<SIZE;i++) printf("%f\n",finalX[i]);
}
